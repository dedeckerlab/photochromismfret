Source code for the separation of FRET pairs based on donor photochromism
====

The software used in this article consists of two parts, and both have been written using IGOR Pro programming language. IGOR Pro 8 can be downloaded (~250Mb) from the Wavemetrics website at https://www.wavemetrics.com/order/order_igordownloads.htm.
This will provide a fully functional version for free during one month. After this period the functionality will be restricted, without paid registration, to a demo version. For this work IGOR Pro 8 (v8.0.5.1) was used on a Windows 10 HOME operating system.

----------------------

The first part of the software consists of the functions that are required to run the analysis on acquired fluorescence data, and which was used to calculate all FRET efficiency values that have been used to create traces and data shown in the figures of the article.

All functions required to perform the analysis can be found in the IGORPro procedure file UnmixingCode_LMFit.ipf and UnmixingCode_Iterative.ipf, which can be opened from within the IGORPro software. All functions are documented within this file.

Additionally, example data is provided in the IGORPro experiment ExampleData.pxp, which contains one dataset comprising average fluorescence intensity traces of one cell expressing two biosensors. This data has  been measured according to the proposed and OFF-switched fluorescence over the course of the experiment.

INSTRUCTIONS: The unmixing analysis on this example dataset can be executed by running RunExampleProcedure_LMFit() or RunExampleProcedure_Iterative() from ExampleUnmixingProc.ipf in the ExampleData.pxp experiment.

OUTPUT:  The output of each analysis will be two IGORPro waves of the converged FRET efficiency time-traces of the two biosensors that are expressed from the cell of which the average intensity timetraces were obtained from.

RUNTIME: Several seconds to minutes


The second part of the software comprises all necessary code to (re)run the simulations that were performed to create the data for the article.

INSTRUCTIONS:
Open a new IGORPro experiment and open the files SimulationProcs.ipf and ProgressViewer.ipf. SimulationProcs.ipf will contain the function Benchmark_intermediates, which can be run using different variables. These variables and other function are further documented within the SimulationProcs.ipf file.

OUTPUT: Violin plot of the Mean Average Deviation (MAD) for the different simulated conditions.

RUNTIME: Several seconds to minutes depending on number of trials
