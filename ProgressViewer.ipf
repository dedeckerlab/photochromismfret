#pragma TextEncoding = "UTF-8"		// For details execute DisplayHelpTopic "The TextEncoding Pragma"
#pragma rtGlobals=3		// Use modern global access method.

#pragma ModuleName=ProgressViewer

Static Function TestProgress()
	variable i, dummy, waituntil
	for (i = 0; i < 100; i+=1)
		waitUntil = StopMSTimer(-2) + 50e3
		ProgressWindow("hello", i, 100)
		do
			dummy += 1
		while (StopMSTimer(-2) < waitUntil)
	endfor
End

// a hierarchic progress window based on that posted to IgorExchange by RGerkin,
// but with entirely separate code and improved functionality

constant kProgressBarWidth = 250
constant kProgressBarHeight = 20
constant kProcessLabelWidth = 100
constant kProcessTextBoxWidth = 100
constant kMargin = 10
constant kButtonHeight = 20
constant kProgressBarMaxDepth = 10
constant kAbortButtonHeight = 20
constant kAbortButtonWidth = 75
constant kTimeRemainingButtonWidth = 200

Static Function ToPixels(val)
	variable val
	// convert a dimension in points to pixels
	return val * (ScreenResolution/72)
End

Static Function ToPoints(val)
	variable val
	// convert a dimension in points to pixels
	return val * (72/ScreenResolution)
End

Function ProgressWindow(processName, num, denom, [allowAbort])
	string processName
	variable num, denom, allowAbort
	
	// if allowAbort is set then execute "Abort" when the user click the abort button
	// if not then return kUserAbort. Default is to abort.
	variable doAbort
	if (ParamIsDefault(allowAbort))
		doAbort = 1
	else
		doAbort = allowAbort
	endif
	
	NewDataFolder /O root:Packages
	NewDataFolder /O root:Packages:ProgressViewer
	DFREF packageDF = root:Packages:ProgressViewer
	
	String /G root:Packages:ProgressViewer:S_DisplayedProcesses
	SVAR displayedProcesses = root:Packages:ProgressViewer:S_DisplayedProcesses
	
	Variable /G root:Packages:ProgressViewer:V_abortRequested
	NVAR abortRequested = root:Packages:ProgressViewer:V_abortRequested
	Variable /G root:Packages:ProgressViewer:V_showTimeRemaining
	NVAR showTimeRemaining = root:Packages:ProgressViewer:V_showTimeRemaining
	
	wave /Z W_LastUpdateTicks = root:Packages:ProgressViewer:W_LastUpdateTicks
	if (WaveExists(W_LastUpdateTicks) == 0)
		// if this wave doesn't exist then none of the other waves exist either
		Make /D/N=0 packageDF:W_LastUpdateTicks, packageDF:W_InitialTicks, packageDF:W_InitialProgress, packageDF:W_PreviousFractionalProgress, packageDF:W_LastDurationEstimateTicks
		Make /D/N=(0, 2) packageDF:M_ProcessProgress
	endif
	wave W_LastUpdateTicks = packageDF:W_LastUpdateTicks
	wave W_InitialTicks = packageDF:W_InitialTicks
	wave W_InitialProgress = packageDF:W_InitialProgress
	wave W_PreviousFractionalProgress = packageDF:W_PreviousFractionalProgress
	wave W_LastDurationEstimateTicks = packageDF:W_LastDurationEstimateTicks
	wave M_ProcessProgress = packageDF:M_ProcessProgress
	
	// the index in the ticks wave is the same as that of processName in displayedProcesses
	variable index
	index = WhichListItem(processName, displayedProcesses)
	if (index == -1)
		// this is a new process, there is no tick count at which it was last called
		// make an entry in the wave
		Redimension /N=(DimSize(W_LastUpdateTicks, 0) + 1) W_LastUpdateTicks, W_InitialTicks, W_InitialProgress, W_PreviousFractionalProgress, W_LastDurationEstimateTicks
		Redimension /N=(DimSize(M_ProcessProgress, 0) + 1, -1) M_ProcessProgress
		index = DimSize(W_InitialProgress, 0) - 1
		// and store the relevant info
		W_LastUpdateTicks[index] = ticks
		W_InitialTicks[index] = ticks
		W_InitialProgress[index] = num
		W_PreviousFractionalProgress[index] = num / denom
		W_LastDurationEstimateTicks[index] = NaN
	else
		if ((ticks - W_LastUpdateTicks[index] < 10) && (num != denom))
			return 0
		else
			W_LastUpdateTicks[index] = ticks
		endif
	endif
	M_ProcessProgress[index][0] = num
	M_ProcessProgress[index][1] = denom
	if (W_PreviousFractionalProgress[index] > num / denom)
		// we're probably starting a new run for this process - reset the timers
		W_InitialTicks[index] = ticks
		W_InitialProgress[index] = num
		W_LastDurationEstimateTicks[index] = NaN
	endif
	W_PreviousFractionalProgress[index] = num / denom
	
	// if processName contains a ':' then that will lead to errors
	if (StringMatch(processName, "*:*"))
		Abort "Error: processName cannot contain a ':')"
	endif
	if ((StringMatch(CleanupName(processName, 0), processName) != 1) || (strlen(processName) >= 30))
		Abort "Error: process names must be valid Igor names and be shorter than 30 characters)"
	endif
	
	if (abortRequested != 0)
		if (doAbort != 0)
			Abort
		else
			return 57	// user abort
		endif
	endif
	
	DoWindow /F ProgressViewer
	if (V_flag != 1)
		// window does not exist
		NewPanel /N=ProgressViewer /W=(300,300, 300 + kProcessLabelWidth + kProgressBarWidth + kProcessTextBoxWidth + 4 * kMargin, 330) /K=1 as "Calculating..."
		Button BTToggleTimeRemaining, win=ProgressViewer, pos={kMargin, kMargin}, size={kTimeRemainingButtonWidth, kAbortButtonHeight}, title="Toggle Time Remaining", proc=BTToggleTimeRemaining
		Button BTAbort, win=ProgressViewer, pos={3 * kMargin + kProcessLabelWidth + kProgressBarWidth, kMargin}, size={kAbortButtonWidth, kAbortButtonHeight}, title="Abort", proc=BTAbortProgressViewer
		Execute /P/Q "KillDataFolder root:Packages:ProgressViewer; DoWindow /K ProgressViewer" // Automatic cleanup of data folder and progress window at the end of function execution.  
	endif
	
	// check if the current process already exists
	// otherwise set up the necessary controls
	variable controlTop, additionalWindowHeight, abortButtonTop
	string controlName
	if (WhichListItem(processName, displayedProcesses) == -1)
		// new process, make room
		if (ItemsInList(displayedProcesses) > kProgressBarMaxDepth)
			Abort "Error: too many processes"
		endif
		if (ItemsInList(displayedProcesses) == 0)
			// when the window is created, Igor gives it a height of at least 30 pixels
			// so that needs to be adapted for
			additionalWindowHeight = ToPoints(kProgressBarHeight + 3 * kMargin + kAbortButtonHeight - 30)
			controlTop = kMargin
			abortButtonTop = controlTop + kProgressBarHeight + kMargin
		else
			GetWindow ProgressViewer, wsize
			additionalWindowHeight = ToPoints(kProgressBarHeight + kMargin)
			controlTop = ToPixels(V_Bottom - V_top) - (kMargin + 20)
			abortButtonTop = controlTop + kMargin + kProgressBarHeight
		endif
		GetWindow ProgressViewer, wsize
		MoveWindow /W=ProgressViewer V_Left, V_Top, V_Right, V_Bottom + ToPoints(additionalWindowHeight)
		controlName = "TBT" +  processName
		TitleBox $controlName, win=ProgressViewer, pos={kMargin, controlTop}, fixedSize=1, size={kProcessLabelWidth, 20}, title=processName
		controlName = "PB" + processName
		ValDisplay $controlName, win=ProgressViewer, pos={2 * kMargin + kProcessLabelWidth, controlTop},size={kProgressBarWidth,kProgressBarHeight},limits={0,1,0},barmisc={0,0}, mode=3
		controlName = "TBV" + processName
		TitleBox $controlName, win=ProgressViewer, pos={3 * kMargin + kProcessLabelWidth + kProgressBarWidth, controlTop}, fixedSize=1, size={kProcessTextBoxWidth, 20}, title="hello"
		Button BTToggleTimeRemaining, win=ProgressViewer, pos={kMargin, abortButtonTop}
		Button BTAbort, win=ProgressViewer, pos={3 * kMargin + kProcessLabelWidth + kProgressBarWidth, abortButtonTop}
		
		displayedProcesses += processName + ";"
	endif
	
	// estimate time remaining for this process
	variable timeRemaining = (ticks - W_InitialTicks[index])/ (num - W_InitialProgress[index]) * (denom -num)
	W_LastDurationEstimateTicks[index] = timeRemaining
	
	// update all of the known processes
	variable nProcesses = ItemsInList(displayedProcesses)
	string progressText 
	variable i, thisProcessNum, thisProcessDenom
	for (i = 0; i < nProcesses; i+=1)
		processName = StringFromList(i, displayedProcesses)
		thisProcessNum = M_ProcessProgress[i][0]
		thisProcessDenom = M_ProcessProgress[i][1]
		controlName = "PB" + processName
		ValDisplay $controlName, win=ProgressViewer, value = _NUM:(thisProcessNum / thisProcessDenom)
		
		if (!showTimeRemaining)
			progressText = num2str(thisProcessNum) + "/" + num2str(thisProcessDenom)
		else
			variable updatedTimeRemaining = (W_LastDurationEstimateTicks[i] - (ticks - W_LastUpdateTicks[i])) / 60
			if (NumType(updatedTimeRemaining) == 0)
				progressText = SecsToTimeStr(limit(updatedTimeRemaining, 0, inf))
			else
				progressText = "???"
			endif
		endif
		controlName = "TBV" + processName
		TitleBox $controlName, win=ProgressViewer, title=progressText
	endfor
	
	DoUpdate /E=1 /W=ProgressViewer
	return 0
End

Function BTAbortProgressViewer(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
				NVAR abortRequested = root:Packages:ProgressViewer:V_abortRequested
				abortRequested = 1
			break
	endswitch

	return 0
End

Function BTToggleTimeRemaining(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
				NVAR showTimeRemaining = root:Packages:ProgressViewer:V_showTimeRemaining
				showTimeRemaining = !showTimeRemaining
			break
	endswitch

	return 0
End


// Code to handle progress updates in MultiThreaded calculations
// Must be called from the main thread
Function UpdateThreadGroupProgress(tgID)
	variable tgID
	
	// see if a progress update is available
	DFREF tgDF = ThreadGroupGetDFR(tgID, 50)	// wait no longer than 50 ms
	if (DataFolderRefStatus(tgFD) == 0)
		return 0
	endif
	
	// a data folder appeared
	// we'll assume that it is meant for this package
	SVAR processName = tgDF:S_processName
	NVAR tasksCompleted = tgDF:V_tasksCompleted
	NVAR tasksRemaining = tgDF:V_tasksRemaining
	
	ProgressWindow(processName, tasksCompleted, tasksRemaining)
End

// Code to handle progress updates in MultiThreaded calculations
// Must be called from the preemptive threads
ThreadSafe Function ReportThreadGroupProgress(processName, tasksCompleted, tasksRemaining)
	string processName
	variable tasksCompleted, tasksRemaining
	
	// create the output data folder
	DFREF outputDF= NewFreeDataFolder()
	
	string /G outputDF:S_processName
	variable /G outputDF:V_tasksCompleted
	variable /G outputDF:V_tasksRemaining
	
	SVAR processNameOut = outputDF:S_processName
	NVAR tasksCompletedOut = outputDF:V_tasksCompleted
	NVAR tasksRemainingOut = outputDF:V_tasksRemaining
	
	processNameOut = processName
	tasksCompletedOut = tasksCompleted
	tasksRemainingOut = tasksRemaining
	
	ThreadGroupPutDF 0, outputDF
End

Function /S SecsToTimeStr(secs)
	variable secs
	
	variable nSecsInMin = 60
	variable nSecsInHour = nSecsInMin * 60
	
	variable nHours = floor(secs / nSecsInHour)
	secs -= nHours * nSecsInHour
	variable nMinutes = floor(secs / nSecsInMin)
	secs -= nMinutes * nSecsInMin
	
	string outStr
	sprintf outStr, "%d:%02d:%02d", nHours, nMinutes, secs
	return outStr
End
