﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.



//// The function FindFRETTracesLM will perform FRET analysis of data comprising one- or two-biosensor fluorescence.
//// Uses three average intensity traces as input corresponding to the signals measured in the three detection channels {S_DD, S_DA, S_AA} over time. 
//// Requires an initial guess for the FRET efficiency of each FRET pair (phiFRET1 & phiFRET2)
//// Will create two waves that contain the FRET efficiency at each timepoint for each FRET pair, respectively (FRET1 & FRET2)
Function FindFRETTracesLM(S_DD, S_DA, S_AA, phiFRET1, phiFRET2)
	wave S_DD, S_DA, S_AA
	variable phiFRET1, phiFRET2
	
	// Make the destination waves that will contain respectively the on- and off-switched data
	Duplicate/FREE S_DD, S_DD_on, S_DD_off, S_DA_on, S_DA_off, S_AA_on, S_AA_off
	Redimension /N=(dimsize(S_DD,0)/2) S_DD_on, S_DD_off, S_DA_on, S_DA_off, S_AA_on, S_AA_off	
	S_DD_on = S_DD[2*p]
	S_DD_off = S_DD[1+2*p]
	S_DA_on = S_DA[2*p]
	S_DA_off = S_DA[1+2*p]
	S_AA_on = S_AA[2*p]
	S_AA_off = S_AA[1+2*p]
		
	// Make the destination wave that will contain FRET efficiency
	variable size = (dimsize(S_DD_on,0))
	Make/FREE/N=(size) phiwave1, phiwave2
	
	// Make a dummy wave that will be used to execute analysis
	Make/FREE/N=(size) dummy
	
		
	// assign the intial value for all points in destination wave
	phiwave1=phiFRET1
	phiwave2=phiFRET1
	
	// execute the photoswitching analysis for each timepoint p
	dummy = LMFindFRET_RealData(S_DD_On[p], S_DD_Off[p], S_DA_On[p], S_DA_Off[p], S_AA[p], phiwave1, phiwave2, p)

	// create the destination waves containing the FRET efficiency for each timepoint
	Duplicate/O phiwave1, FRET1
	Duplicate/O phiwave2, FRET2
	
	// Kill unnecessary waves
	Killwaves/Z dummy, phiwave1, phiwave2, S_DD_on, S_DD_off, S_DA_on, S_DA_off, S_AA_on, S_AA_off	
End










// Function that performes the photoswitching analysis for the timepoint 'point' 
// This function is best using as part of the function 'FindFRETTracesLM' to calculate the values at every timepoint.
//
// The input requires the measured S_DD, S_DA and S_AA intensity after on- and off-switching, respectively, 
// (W_S_DD_On, W_S_DD_Off, W_S_DA_On, W_S_DA_Off, W_S_AA).
// S_AA only requires only one value (after on-switching) since it is independent of the switching process.
// The calculation also requires an input wave for the FRET efficiency for each FRET pair (phiwave1 & phiwave2)
// of the same dimensions as the input intensity waves. 
// This wave should contain initial guess for the FRET efficiency at each timepoint, and will
// contain the determined FRET efficiency values at the end of the calculation.
Function LMFindFRET_RealData(W_S_DD_On, W_S_DD_Off, W_S_DA_On, W_S_DA_Off, W_S_AA, phiwave1, phiwave2, point)
	variable W_S_DD_On, W_S_DD_Off, W_S_DA_On, W_S_DA_Off, W_S_AA, point
	wave phiwave1, phiwave2
	variable phiFRET1, phiFRET2
	
	// set the starting guess for the FRET-efficiency
	// for optimal performance, we suggest here to use the value that was calculated for the previous timepoint
	phiFRET1 = phiwave1[max(point-1,0)]
	phiFRET2 = phiwave2[max(point-1,0)]


	// The wave W_FitCoeffs contains the parameters that we want to Fit, 
	// We additionally set physical constraints to enhance the performance of the fit
	// W_Constraints contains the matching constraints
	// W_FitCoeffs[0] = S_AA_underlying	(no constraint)
	// W_FitCoeffs[1] = W_S_DD_On_1		(needs to be positive)
	// W_FitCoeffs[2] = W_S_DD_On_2		(needs to be positive)
	// W_FitCoeffs[3] = phiFRET1			(needs to be between 0 and 1)
	// W_FitCoeffs[4] = phiFRET2			(needs to be between 0 and 1)
	//
	// as an initial guess we are taking a 50/50 contribution of both sensors
	Make /T/O W_Constraints = {"K1 >= "+num2str(W_S_DD_On/100), "K2 >= "+num2str(W_S_DD_On/100), "K3 >= 0.01", "K3 <= 0.99","K4 >= 0.01", "K4 <= 0.99"}
	Make /D/FREE W_FitCoeffs = {W_S_AA, W_S_DD_On/2, W_S_DD_On/2, phiFRET1, phiFRET2}	
	
	// W_FitY contains the raw data, W_FitX is used for bookkeeping
	Make /N=(5)/D/FREE W_FitY, W_FitX
	W_FitY[0] = W_S_DD_On 	// (W_FitX[0] = 0)
	W_FitY[1] = W_S_DD_Off	// (W_FitX[1] = 1)
	W_FitY[2] = W_S_DA_On	// (W_FitX[2] = 2)
	W_FitY[3] = W_S_DA_Off	// (W_FitX[3] = 3)
	W_FitY[4] = W_S_AA		// (W_FitX[4] = 4)
	W_FitX = p
	
	// do the fit
	variable V_FitError = 0
	
	// we will be fitting W_FitY (the raw data) with the function LMFindFRET_RealDataDialog optimizing the parameters in W_FitCoeffs
	FuncFit /N=1 /Q LMFindFRET_RealDataDialog, W_FitCoeffs, W_FitY /X=W_FitX /C=W_Constraints
	if (V_FitError != 0)
		if (V_FitError == 3)	// singular matrix. This might occur using bad initial guesses for the fitting constraints.
			phiFRET1 = 2
			phiFRET2 = 2
			phiwave1[point] = phiFRET1
			phiwave2[point] = phiFRET2
			return 0 
		else								
			phiFRET1 = 3			// Other error. This might occur using bad initial guesses for the fitting constraints.
			phiFRET2 = 3
			phiwave1[point] = phiFRET1
			phiwave2[point] = phiFRET2
			return 0
		endif
	endif
	
	// assign the values to the destination wave
	phiwave1[point] = W_FitCoeffs[3] 	// FRET efficiency FRET pair 1
	phiwave2[point] = W_FitCoeffs[4]		// FRET efficiency FRET pair 2
	
	
	// kill unnecessary waves
	Killwaves/Z W_FitCoeffs, W_FitY, W_FitX, M_CMatrix, W_DVector
End



// This is the orthogonal least-squares distance fitting function. 
// It will require additional waves in the root: folder to function properly:
// dependency_D1			wave describing the FRET efficiency in function of rho for donor emission of FRET pair 1
// dependency_D2			wave describing the FRET efficiency in function of rho for donor emission of FRET pair 2
// dependency_A1			wave describing the FRET efficiency in function of rho for acceptor emission of FRET pair 1
// dependency_A2			wave describing the FRET efficiency in function of rho for acceptor emission of FRET pair 2
// crosstalkcoefs	wave containing the crosstalk coefficients = {alpha1, alpha2, beta, gamma1, gamma2}
Function LMFindFRET_RealDataDialog(w,t) : FitFunc
	wave w
	variable t
	variable y
	
	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(t) = S_DD_On_1 + S_DD_On_2 + phiFRET1 + phiFRET2 + t
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ t
	//CurveFitDialog/ Coefficients 5
	//CurveFitDialog/ w[0] = S_AA
	//CurveFitDialog/ w[1] = S_DD_On_1
	//CurveFitDialog/ w[2] = S_DD_On_2
	//CurveFitDialog/ w[3] = phiFRET1
	//CurveFitDialog/ w[4] = phiFRET2
	
	
	// assign variables that contain the values of the chosen fit coefficients
	variable S_AA = w[0] 			// S_AA underlying
	variable S_DD_On_1 = w[1]
	variable S_DD_On_2 = w[2]
	variable phiFRET1 = w[3]
	variable phiFRET2 = w[4]
	
	// declare hard numbers based on waves in root folder:
	wave dependency_D1 = root:dependency_D1 // dependency pair 1
	wave dependency_A1 = root:dependency_A1
	wave dependency_D2 = root:dependency_D2 // dependency pair 2
	wave dependency_A2 = root:dependency_A2
	
	wave crosstalkcoeffs = root:crosstalkcoeffs  // crosstalkcoefficients
	variable c_alpha1 = crosstalkcoeffs[0]
	variable c_alpha2 = crosstalkcoeffs[1]
	variable c_beta = crosstalkcoeffs[2]
	variable c_gamma1 = crosstalkcoeffs[3]
	variable c_gamma2 = crosstalkcoeffs[4]
	
	
	// find rhoD1, rhoD2, rhoA1 & rhoA2 using dependency functions and guesses for phiFRET1 and phiFRET2
	Make /FREE/D /N=(1) inwave, outwave
	
	inwave = 	phiFRET1
	Duplicate/FREE/O CalculateRhoFromFRET(inwave, dependency_D1) outwave
	variable rhoD1 = outwave[0]
	
	inwave = phiFRET2
	Duplicate/FREE/O CalculateRhoFromFRET(inwave, dependency_D2) outwave
	variable rhoD2 = outwave[0]
	
	inwave = 	phiFRET1
	Duplicate/FREE/O CalculateRhoFromFRET(inwave, dependency_A1) outwave
	variable rhoA1 = outwave[0]
	
	inwave = phiFRET2
	Duplicate/FREE/O CalculateRhoFromFRET(inwave, dependency_A2) outwave
	variable rhoA2 = outwave[0]
		
	if (!((t==1) || (t==3))) // set rho to 1 for acquisitions after on-switching
		rhoD1 = 1	
		rhoD2 = 1
		rhoA1 = 1
		rhoA2 = 1
	endif
	
	// calculate the emission based on guesses for the fit coefficients:
	variable S_DD_1 = S_DD_On_1*rhoD1
	variable S_DD_2 = S_DD_On_2*rhoD2
	variable S_Fc_1 = c_gamma1*S_DD_on_1/(1/phiFRET1-1)*rhoA1
	variable S_Fc_2 = c_gamma2*S_DD_on_2/(1/phiFRET2-1)*rhoA2
	
	variable S_DD_summed = S_DD_1 + S_DD_2
	variable S_DA_summed = S_Fc_1 + S_Fc_2 + c_alpha1*S_DD_1 + c_alpha2*S_DD_2 + c_beta*S_AA	
	
	// return to Y
	if (t==0)
		return S_DD_summed		// W_S_DD_On
	elseif (t==1)
		return S_DD_summed		// W_S_DD_Off
	elseif (t==2)
		return S_DA_summed		// W_S_DA_On
	elseif (t==3)
		return S_DA_summed		// W_S_DA_Off
	elseif (t==4)
		return S_AA				// W_S_AA
	endif	
End






///////////////////////////////////////////
// ADDITIONAL FUNCTIONS USED IN ANALYSIS:
///////////////////////////////////////////


// Returns a wave with corresponding switching ratio's rho 
// based on input FRET response trace 'inputwave' and dependence function 'fitfunc'
// 'fitfunc' describes the relation between FRET (Y) and rho (X)
Function/WAVE CalculateRhoFromFRET(inputwave, fitfunc)
	wave inputwave,  fitfunc
	
	WaveStats/Q fitfunc
	variable fitfuncmax = V_max
	variable fitfuncmin = V_min
	
	Duplicate/FREE inputwave RhoOutputwave
	variable i
	variable size = dimsize(inputwave,0)
	for (i=0;i<size;i+=1)
		
		variable currentvalue = inputwave[i]
		if (currentvalue <= fitfuncmin)
			FindLevel/Q fitfunc, fitfuncmin
			RhoOutPutWave[i] = V_LevelX
		elseif (currentvalue >= fitfuncmax)
			FindLevel/Q fitfunc, fitfuncmax
			RhoOutPutWave[i] = V_LevelX
		else		
			FindLevel/Q fitfunc, inputwave[i]
			RhoOutputWave[i] = V_LevelX
		endif	
	endfor	
	return RhoOutputwave
End