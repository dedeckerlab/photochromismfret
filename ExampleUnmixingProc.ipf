﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

///////// EXAMPLE PROCEDURE //////////////
//////////////////////////////////////////
//////////////////////////////////////////
// The following example uses example data calculated from three images acquired in the three detection channels (S_DD,S_DA,S_AA). The images here correspond
// to cells expressing rsAKARev and EKARev biosensors, and are an example dataset used in the analysis and creation of Fig 5 in the main article.
// Average intensity traces and background subtraction was performed using these images and one region of interest and a background region.
//
// Furthermore, it uses predetermined FRET-rho dependencies for donor and acceptor emission (dependency_D1, dependency_D2, dependency_A1, dependency_A2), 
// which characterize the FRET efficiciency dependence (Y) on rho (X).  
// 
// To run the example procedure, execute RunExampleProcedure() from the commandline.
// Rationale behind the analysis is explained further below.
//
// All required data can be found in the root folder.
///////////////////////////////////////////



// Runs an example analysis on the dataset provided in this experiment file using nonlinear fitting.
Function RunExampleProcedure()
	SetDataFolder root:
	wave S_DD_trace, S_DA_trace, S_AA_trace
	
	// some variables that will be required for the analysis:
	variable phiFRET1_guess = 0.25
	variable phiFRET2_guess = 0.25	
	
	FindFRETTracesLM(S_DD_trace, S_DA_trace, S_AA_trace, phiFRET1_guess, phiFRET2_guess)
	
	Display; 
	Appendtograph root:FRET1, root:FRET2
End